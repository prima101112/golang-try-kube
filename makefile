build:
	@go build -o ./bin/main .

install:
	go build -o ./bin/main .
	mkdir /usr/local/share/main-try-cube
	cp ./bin/main /usr/local/share/main-try-cube/

run:
	@go build -o ./bin/main.local .
	@./bin/main.local