package main

import (
	"fmt"
	"log"
	"net/http"
)

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func about(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "learn kube")
}

func main() {
	http.HandleFunc("/about", about)
	http.HandleFunc("/ping", ping)
	http.HandleFunc("/", index)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
